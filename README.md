# Test haibu
 
# Init project 

* Docker:
    * Rename file env copy to .env
    * run
    ```bash
        docker-compose -up
    ```
    * Check endpoints on the browser 
    * url: http://localhost:5000/
* normal:
    * install python3.7
    * install all dependencies 
    ```bash
        pip install -r requirements.txt
    ```
    * Check endpoints on the browser 
    * url: http://localhost:5000/

# Enpoint 

* [__GET__]http://localhost:5000/getPatenteByID/<__ID__>
    * return a dict object with the id and the patente by id given
    Example: 
    ```bash
        http://localhost:5000/getPatenteByID/3
        return {
            "id": "3", 
            "patente": "AAAA003"
            }
    ```

* [__GET__]http://localhost:5000/getIdByPatente/<__PATENTE__>
    * return a dict object with the PATENTE and the id by PATENTE given 
    Example: 
    ```bash
        http://localhost:5000/getIdByPatente/AAAA003
        return {
            "id": 3, 
            "patente": "AAAA003"
            }
    ```
* [__POST__]http://0.0.0.0:5000/matriz/
    * return a dict object with the Matrix,subMatrix and sum for de sub Matrix created
    Example: 
    ```bash
        http://localhost:5000/getIdByPatente/AAAA003
        data = {
            'R':1,
            'C':4,
            'Z':2,
            'X':1,
            'Y':3
        }
        return {'main_matriz': [2, 2, 2, 2], 'sub_matriz': [2, 2, 2], 'sum_matriz': 6}"
    ```
    See the exmaple in testMatriz.py 
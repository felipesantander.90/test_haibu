FROM python:3.7-slim
RUN mkdir /code
WORKDIR /code
COPY . /code/
RUN pip install -r requirements.txt
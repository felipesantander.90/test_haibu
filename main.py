from flask import Flask
from flask import request
from string import ascii_uppercase as ALC
from itertools import chain, product
import numpy as np

app = Flask(__name__)

count = 1
final_dict = {}
min = 0
max = 999
digits = [ str(i) for i in range(min,max)]
digits = [(len(str(max))-len(digit))*'0'+digit for digit in digits]
patentes = [''.join([a,a,a,a,digit]) for a in ALC for digit in digits]
ids = {patentes[i]:i for i in range(0,len(patentes))}



@app.route('/')
def hello():
    R= 4
    C= 4
    Z= 2
    X= 2
    Y= 2
    def f(x,y):
        return Z+(x-1)
    b = np.fromfunction(f, (R, C), dtype=int)
    print(b)
    sub_b = b[0:X,0:Y]
    return str(sub_b.sum())

@app.route('/getPatenteByID/<id>')
def get_patente_by_id(id):
    try:
        index = int(id)
        if index>len(patentes)-1:
            return 'indice sin patente'
        return {
            'id': id,
            'patente': patentes[index]
        }
    except:
        return 'no es un id valido'

@app.route('/getIdByPatente/<patente>')
def get_id_by_patente(patente):
    return {
        'patente':patente,
        'id': ids.get(patente,'No eexiste id asociado a esa patente') 
    }

@app.route('/matriz/', methods = ['POST'])
def matriz():
    data = request.form
    try:
        R = int(data.get('R'))
        C = int(data.get('C'))
        Z = int(data.get('Z'))
        X = int(data.get('X'))
        Y = int(data.get('Y'))
    except:
        return 'uno o mas valores no son enteros '

    if R<=0 or C<=0 or Z<=0:
        return 'Los valores de R o C o Z deben ser mayores a cero'
    if X<0 or Y<0:
        return 'Los valores de X e Y deben ser mayores o iguales a cero'
    if Z> 1000000:
        return 'El valor de Z no debe sobrepasar los 1.000.000'
    #definimos la funcion para crear la matrix con parametros:  Z + Rn -1
    if X>R:
        return 'El punto X esta fuera de la matriz'
    if Y>C:
        return 'El punto Y esta fuera de la matriz'

    def f(x,y):
        return Z+(x-1)
    if R == 1:
        b = [Z]*C
        sub_b = b[0:Y]
        sum_total = sum(sub_b)
    else:
        # Se crea la matriz primaria 
        b = np.fromfunction(f, (R, C), dtype=int)
        #creamos la sub matris bajo X e Y   
        sub_b = b[0:X,0:Y]
        # Sumamos la matriz
        sum_total = sub_b.sum()

    response = {
        'main_matriz': b,
        'sub_matriz': sub_b,
        'sum_matriz': sum_total
    }

    return str(response)




if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=5000)